" Vim-Scratchpad

" Exit if vim-scratchpad is already loaded
if exists("g:loaded_Scratchpad")
    finish
endif
let g:loaded_Scratchpad = 1

" Default width: 20% of window-size
if !exists('g:scratchpad_width')
	let g:scratchpad_width = 0.2
endif

" Default scratchpad file: .scratch.md
if !exists('g:scratchpad_file')
	let g:scratchpad_file = '.scratchpad'
endif

" Command to toggle Scratchpad
command! -nargs=0 ScratchpadToggle call Scratchpad#Toggle()

" vim: tabstop=2
