# Vim-scratchpad

Light-weight scratchpad for Vim.

## Features

* Open a scratchpad for easy note-taking.
* Markdown highlights.
* Scratchpad persists across vim sessions.
* Configurable width and file location.
* Very light-weight.

## Usage

* Toggle the scratchpad using `:ScratchpadToggle`.
* Map the command for easy toggle. For example, to use `gs`, add the following
  to your vimrc:

```vim
nnoremap gs :ScratchpadToggle<CR>
```
* The default width of the scratchpad window is 20% of the vim-window. Control
  it using `g:scratchpad_width`. For example, to make it 30%:

```vim
let g:scratchpad_width = 0.3
```
* By default, a (hidden) scratchpad is created in the current directory
  (`.scratchpad`). To change its name or location, define `g:scratchpad_file`.
  For example, the following setting will save a single scratchpad in your
  `$HOME` directory:

```vim
let g:scratchpad_file = '~/.scratchpad'
```

For more features, check out [mtth](https://github/com/mtth)'s [scratch.vim](https://github.com/mtth/scratch.vim).

## Installation

Use your favorite plugin-manager, or install manually.
Refer [this article](https://gist.github.com/manasthakur/ab4cf8d32a28ea38271ac0d07373bb53)
for general help on managing plugins in Vim.

[Star this repository](https://github.com/manasthakur/vim-scratchpad/) on GitHub if you like the plugin.
Feel free to send bricks and bouquets to `manasthakur17@gmail.com`.

## License

[MIT](LICENSE)

