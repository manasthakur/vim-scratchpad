" Vim-Scratchpad

" Local function to open scratchpad
function! s:open()
	execute 'rightbelow' . float2nr(g:scratchpad_width * winwidth(0)) . 'vsplit' . g:scratchpad_file
	execute 'setlocal filetype=markdown'
	setlocal winfixheight
	setlocal winfixwidth
	setlocal nobuflisted
	augroup Scratchpad
		autocmd!
		execute 'autocmd WinEnter <buffer=' . bufnr(g:scratchpad_file) . '> call Scratchpad#Close()'
	augroup END
endfunction

" Close Scratchpad if it's the last window open
function! Scratchpad#Close()
	if bufwinnr(g:scratchpad_file) != -1
		if winnr('$') == 1
			quit
		endif
	endif
endfunction

" Toggle scratchpad
function! Scratchpad#Toggle()
	let scr_winnr = bufwinnr(g:scratchpad_file)
	if scr_winnr != -1
		execute scr_winnr . 'close'
	else
		call s:open()
	endif
endfunction

" vim: tabstop=2
